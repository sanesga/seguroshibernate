package app;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import model.AsistenciaMedica;
import model.Coberturas;
import model.Direccion;
import model.Enfermedades;
import model.Nif;
import model.Seguro;
import model.Sexo;
import model.TipoAsistencia;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Crea un nuevo programa similar al Ejemplo01 que realice las operaciones de
 * insertar, leer, actualizar y borrar de la entidad Seguro usando los ficheros
 * ”.hbm.xml” de Hibernate.
 */
public class main {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        Seguro s1 = null;
        Direccion d = null;
        int opcion;
        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        Set<AsistenciaMedica> asistenciasMedicas = new HashSet<>();
        AsistenciaMedica am1,am2;
        Nif nif;
        Enfermedades e;
        Coberturas c;
        BigDecimal bd1,bd2;
        String texto1,texto2;
        Sexo sexo = null;
        TipoAsistencia tipoAsistencia = null;
        boolean casado, embarazada;
        
        //configuration.addClass(Seguro.class);
        configuration.configure();
        
        sessionFactory = configuration.buildSessionFactory();

        //creamos una sesion
        Session session = sessionFactory.openSession();

        do {

            System.out.println("Elige opción");
            menu();
            opcion = teclado.nextInt();

            switch (opcion) {
                case 1://CREAR SEGURO
                    nif= new Nif("123456V");
                    e= new Enfermedades(false,false,false,true,"polvo");
                    c= new Coberturas(true,true,false);
                    s1 = new Seguro(nif , "Juan", "Cano", "Morales", 38, 3,c,e,sexo.HOMBRE,true,true);
                    d = new Direccion (7, "Calle la Reina", 5, "Xàtiva", "Valencia");
                    bd1 = new BigDecimal("5.5");
                    bd2 = new BigDecimal("10.2");
                    texto1="Una temperatura normal puede variar de persona a persona, pero generalmente es alrededor de 98.6 ºF o 37ºC. La fiebre no es una enfermedad. Por lo general, es una señal de que su cuerpo está tratando de combatir una enfermedad o infección. Las infecciones causan la mayoría de las fiebres.";
                    texto2="Los signos y síntomas de la apendicitis pueden comprender los siguientes: Dolor repentino que comienza en el lado derecho de la parte inferior del abdomen. Dolor repentino que comienza alrededor del ombligo y, a menudo, se desplaza hacia la parte inferior derecha del abdomen";
                    am1 = new AsistenciaMedica(s1,"Ir al médico de cabecera por fiebre", "Valencia",texto1,bd1, tipoAsistencia.AMBULATORIA);
                    am2 = new AsistenciaMedica(s1,"Operación de apendicitis", "Castellón",texto2,bd2, tipoAsistencia.CENTROSALUD);
                    asistenciasMedicas.add(am1);
                    asistenciasMedicas.add(am2);
                    s1.setAsistenciasMedicas(asistenciasMedicas);
                    s1.setDireccion(d);
                    System.out.println("Seguro creado con éxito");
                    break;
                case 2://GUARDAR SEGURO
                    session.beginTransaction();
                    session.save(s1);
                    session.getTransaction().commit();
                    System.out.println("Seguros guardado con éxito");
                    break;
                case 3://LEER SEGURO
                    s1 = session.get(Seguro.class, 51);
                    System.out.println(s1);
                    break;
                case 4://ACTUALIZAR
                    session.beginTransaction();
                    s1.setApe1("Rodríguez");
                    session.update(s1);
                    session.getTransaction().commit();
                    System.out.println("Seguro actualizado con éxito");
                    break;
                case 5://ELIMINAR SEGURO
                    session.beginTransaction();
                    session.delete(s1);
                    session.getTransaction().commit();
                    System.out.println("Seguro eliminados con éxito");
                    break;
                case 6://SALIR
                    //cerrar la conexion
                    session.close();
                    sessionFactory.close();
                    System.out.println("Saliendo..");
                    break;
                default:
                    throw new AssertionError();
            }
        } while (opcion != 6);
    }

    public static void menu() {
        System.out.println("CONSIDERACIÓN 1. EJECUTAR LOS PASOS POR ORDEN");
        System.out.println("CONSIDERACIÓN 2. RECORDAR SALIR DE LA APLICACIÓN PARA CERRAR SESIÓN");
        System.out.println("CONSIDERACIÓN 3. SI SE INTENTA VOLVER A AÑADIR LOS SEGUROS, COMO SON FIJOS DARÁ ERROR");
        System.out.println("CONSIDERACIÓN 4. BORRAR EL SEGURO ANTES DE SALIR, PARA PODER UTILIZAR LOS MISMOS DATOS MÁS TARDE");
        System.out.println("1. Crear seguro");
        System.out.println("2. Guardar seguro");
        System.out.println("3. Leer seguro");
        System.out.println("4. Actualizar seguro");
        System.out.println("5. Eliminar seguro");
        System.out.println("6. Salir");
    }
}
